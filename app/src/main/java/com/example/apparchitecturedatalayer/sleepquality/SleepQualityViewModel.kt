package com.example.apparchitecturedatalayer.sleepquality

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.apparchitecturedatalayer.database.SleepDatabaseDao
import kotlinx.coroutines.*

class SleepQualityViewModel(
    private val nightId: Long,
    private val dataSource: SleepDatabaseDao
) : ViewModel() {

    init {

    }

    private val viewmodelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewmodelJob)

    override fun onCleared() {
        super.onCleared()
        viewmodelJob.cancel()
    }

    // "event" snippet for navigating
    private val _navigateToSleepTracker = MutableLiveData<Boolean?>()
    val navigateToSleepTracker: LiveData<Boolean?> get() = _navigateToSleepTracker
    fun doneNavigatingToSleepTracker () {
        _navigateToSleepTracker.value = null
    }

    fun onSetSleepQuality(quality: Int) {
        uiScope.launch {
            withContext(Dispatchers.IO) {
                val tonight = dataSource.getOneNightById(nightId) ?: return@withContext
                tonight.sleepQuality = quality
                dataSource.update(tonight)
            }
            _navigateToSleepTracker.value = true
        }
    }
}