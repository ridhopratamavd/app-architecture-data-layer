package com.example.apparchitecturedatalayer.database

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface SleepDatabaseDao {

    @Insert
    fun insert(night: SleepNight)

    @Update
    fun update(night: SleepNight)

    @Query("SELECT * FROM daily_sleep_quality_table WHERE nightId = :key")
    fun getOneNightById(key: Long) : SleepNight

    @Query("DELETE FROM daily_sleep_quality_table")
    fun clear()

    /* returning live data, amazing feature of rrom. room make sure live data updated whenever database is updated
    * hanya perlu get all night once, attach observer, dan saat di db berubah, UI akan berubah tanpa perlu get lagi
    * save time, code complexity
    * */
    @Query("SELECT * FROM daily_sleep_quality_table ORDER BY nightId DESC")
    fun getAllNights() : LiveData<List<SleepNight>>


    /*
    * returning nullable, karena mungkin jaga2 kalo gaada data di db (di clear),*/
    @Query("SELECT * FROM daily_sleep_quality_table ORDER BY nightId DESC LIMIT 1")
    fun getTonight() : SleepNight?
}