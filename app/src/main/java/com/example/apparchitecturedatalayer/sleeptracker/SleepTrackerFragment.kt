package com.example.apparchitecturedatalayer.sleeptracker

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.apparchitecturedatalayer.R
import com.example.apparchitecturedatalayer.database.SleepDatabase
import com.example.apparchitecturedatalayer.databinding.FragmentSleepTrackerBinding

import com.google.android.material.snackbar.Snackbar
import timber.log.Timber

/**
 * A fragment with buttons to record start and end times for sleep, which are saved in
 * a database. Cumulative data is displayed in a simple scrollable TextView.
 * (Because we have not learned about RecyclerView yet.)
 */
class SleepTrackerFragment : Fragment() {

    /**
     * Called when the Fragment is ready to display content to the screen.
     *
     * This function uses DataBindingUtil to inflate R.layout.fragment_sleep_quality.
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Get a reference to the binding object and inflate the fragment views.
        val binding: FragmentSleepTrackerBinding = DataBindingUtil.inflate(
            inflater, R.layout.fragment_sleep_tracker, container, false
        )

        val application = requireNotNull(this.activity).application
        val dataSource = SleepDatabase.getInstance(application).sleepDatabaseDao
        val viewModelFactory = SleepTrackerViewModelFactory(dataSource, application)
        val viewModel =
            ViewModelProvider(this, viewModelFactory).get(SleepTrackerViewModel::class.java)
        binding.setLifecycleOwner { lifecycle }
        binding.sleepTrackerViewModel = viewModel

        /* event */
        viewModel.navigateToSleepQuality.observe(viewLifecycleOwner, Observer { night ->
            if (night !== null) {
                Timber.i("navigateToSleepQuality %d", night.nightId)
                this.findNavController().navigate(
                    SleepTrackerFragmentDirections.actionSleepTrackerFragmentToSleepQualityFragment(
                        night.nightId
                    )
                )
                viewModel.doneNavigatingToSleepQualityFragment()
            }
        })

        /*event show snackbar*/
        viewModel.showSnackBarEvent.observe(viewLifecycleOwner, Observer {
            if(it == true) {
                Snackbar.make(requireActivity().findViewById(android.R.id.content),getString(R.string.cleared_message),Snackbar.LENGTH_LONG).show()
                viewModel.doneShowingSnackBar()
            }
        })
        return binding.root
    }
}
