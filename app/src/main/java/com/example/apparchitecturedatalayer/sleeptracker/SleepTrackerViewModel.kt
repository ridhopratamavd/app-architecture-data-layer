package com.example.apparchitecturedatalayer.sleeptracker

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.example.apparchitecturedatalayer.database.SleepDatabaseDao
import com.example.apparchitecturedatalayer.database.SleepNight
import com.example.apparchitecturedatalayer.formatNights
import kotlinx.coroutines.*
import timber.log.Timber

class SleepTrackerViewModel(
    val database: SleepDatabaseDao,
    application: Application
) : AndroidViewModel(application) {


    //    to manage coroutine (seperti cancel all coroutine saat viewmodel nolonger use dan destroyed)
    private var viewModelJob = Job()

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

    //   scope determine what thread the coroutine will run on and its need to know about the job
    // this means coroutine launch in the ui scope will run on main thread
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    //    nullable when no data exist
    private var tonight = MutableLiveData<SleepNight?>()

    private val nights = database.getAllNights()

    init {
        initializeTonight();
    }

    private fun initializeTonight() {
        Timber.i("initializetonight")
        uiScope.launch {
            tonight.value = getTonightFromDatabase()
        }
    }

    private suspend fun getTonightFromDatabase(): SleepNight? {
        Timber.i("getTonightFromDatabase")
        return withContext(Dispatchers.IO) {
            Timber.i("getTonightFromDatabase with context")
            var night = database.getTonight()
            if (night?.endTimeMilli != night?.startTimeMilli) {
                night = null
            }
            night
        }
    }

    fun onStartTracking() {
        Timber.i("onstarttracking")
        uiScope.launch {
            Timber.i("onstarttracking launch")
            val newNight = SleepNight()

            insert(newNight)
            tonight.value = getTonightFromDatabase()
        }
    }

    private suspend fun insert(newNight: SleepNight) {
        Timber.i("insert")
        withContext(Dispatchers.IO) {
            Timber.i("insert withcontext")
            database.insert(newNight)
        }
    }

    fun onStopTracking() {
        uiScope.launch {
            val oldNight = tonight.value ?: return@launch
            oldNight.endTimeMilli = System.currentTimeMillis()

            update(oldNight)
            _navigateToSleepQuality.value = oldNight
        }
    }

    private suspend fun update(oldNight: SleepNight) {
        withContext(Dispatchers.IO) {
            database.update(oldNight)
        }
    }

    fun onClear() {
        uiScope.launch {
            clear()
            tonight.value = null
            _showSnackBarEvent.value = true
        }
    }

    private suspend fun clear() {
        withContext(Dispatchers.IO) {
            database.clear()
        }
    }

    val nightString =
        Transformations.map(nights) { nights -> formatNights(nights, application.resources) }

    //    encapsulate mutable livedata
    private val _navigateToSleepQuality = MutableLiveData<SleepNight>()
    val navigateToSleepQuality: LiveData<SleepNight> get() = _navigateToSleepQuality
    fun doneNavigatingToSleepQualityFragment() {
        _navigateToSleepQuality.value = null
    }

    val startButtonVisibility = Transformations.map(tonight) {
        null == it
    }

    val stopButtonVisibility = Transformations.map(tonight) {
        null !== it
    }

    val clearButtonVisibility = Transformations.map(nights) {
        it?.isNotEmpty()
    }

    // snackbar event
    private val _showSnackBarEvent = MutableLiveData<Boolean>()
    val showSnackBarEvent: LiveData<Boolean> get() = _showSnackBarEvent
    fun doneShowingSnackBar() {
        _showSnackBarEvent.value = false;
    }
}