package com.example.apparchitecturedatalayer

import android.app.Application
import timber.log.Timber

class AppArchitectureDataLayer :Application() {
    override fun onCreate() {
        super.onCreate()
        Timber.plant(Timber.DebugTree())
    }
}